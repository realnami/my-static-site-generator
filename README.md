# My Static Site Generator

A easy static site generator.


## Dependence

* djot
* python3-jinja2
* make


## How to use


### Add new file.

1. Add a file.djot in source folder.
2. Any time add new djot file in source folder, you must run configure.
3. Run command make.


### Edit file.

1. Edit a file in source.
2. Run command make.


### Remove file.

1. Remove a file in source.
2. Run configure script.
3. Run command make.


## Custom css

Just modify style.css in public/static.


## Custom template

Just modify template.html in template.