#!/usr/bin/env python3
import sys
import signal

import jinja2


def timeout_handler():
  raise Exception("stdin timeout")


signal.signal(signal.SIGALRM, timeout_handler)
signal.alarm(5)


templateLoader = jinja2.FileSystemLoader(searchpath = "./template")
templateEnv = jinja2.Environment(loader = templateLoader)
TEMPLATE_FILE = "template.html"
template = templateEnv.get_template(TEMPLATE_FILE)


config = {"main_page" : sys.stdin.read()}
outputText = template.render(config)

print(outputText)
