include Makefile.dep

DJOT=djot


all: public/static $(DIR_DEP) $(DEP)


public/%.html: source/%.djot
	$(DJOT) $< | tools/process-djot.py > $@

public/static:
	cp -r static $@

public/static/style.css: static/style.css
	cp -r $< $@

$(DIR_DEP):
	mkdir -p $@


.PHONY: clean
clean:
	find public -path public/static -prune -o -type f -exec rm {} \;
